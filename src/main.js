"use strict";
var React = require('react');
var Router = require('react-router');
var routes = require('./routes');

// For clean URL use Router.HistoryLocation as second param in the below function
//Router.run(routes, Router.HistoryLocation, function(Handler){
Router.run(routes, function(Handler){
	React.render(<Handler/>, document.getElementById('app'));
});