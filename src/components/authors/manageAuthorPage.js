"use strict";
var React = require('react');
var Router = require('react-router');
var toastr = require('toastr');
var AuthorForm = require('./authorForm');
var AuthorApi = require('../../api/authorApi');

var ManageAuthorPage = React.createClass({

	statics: {
			willTransitionFrom: function(transition, component){
				if (component.state.dirty && !confirm('Leave without saving?')) {
					transition.abort();
				}
			}
	},

	mixins: [
		Router.Navigation
	],

	getInitialState: function() {
		return {
			author: { id: '', firstName: '', lastName: ''},
			errors: {},
			dirty: false
		};
	},

	componentWillMount: function() {
		var authorId = this.props.params.id; // This is from the path '/author:id'

		if (authorId) {
			this.setState({author: AuthorApi.getAuthorById(authorId)});
		}
	},

	setAuthorState: function(event) {
		this.setState({dirty: true});
		var field = event.target.name;
		var value = event.target.value;
		this.state.author[field] = value;
		return this.setState({author: this.state.author});
	},

	authorFormIsValid: function() {
		var isFormISValid = true;
		this.state.errors = {}; // To clear  if any privous errors

		if (this.state.author.firstName.length < 3) {
			this.state.errors.firstName = 'FirstName must be atleast 3 charecters';
			isFormISValid = false;
		}
		if (this.state.author.lastName.length < 3) {
			this.state.errors.lastName = 'LastName must be atleast 3 charecters';
			isFormISValid = false;
		}

		this.setState({errors: this.state.errors});
		return isFormISValid;
	},

	saveAuthor: function(event) {
		event.preventDefault();
		if (!this.authorFormIsValid()) {
			return;
		}
		AuthorApi.saveAuthor(this.state.author);
		this.setState({dirty: false});
		toastr.success('Author Saved');
		this.transitionTo('authors');
	},
	render: function() {
		return (
			<div>
				<AuthorForm 
					author={this.state.author} 
					onChange={this.setAuthorState}
					onSave={this.saveAuthor}
					errors={this.state.errors} />
			</div>
		);
	}
});

module.exports = ManageAuthorPage;