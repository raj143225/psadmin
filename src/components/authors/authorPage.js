"use strict";

var React = require('react');
var AuthorList = require('./authorList');
var AuthorApi = require('../../api/authorApi.js');
var Router = require('react-router');
var Link = Router.Link;

var AuthorPage = React.createClass({

	// wait: function(ms){
	// 	var start = new Date().getTime();
	//    	var end = start;
	//    	while(end < start + ms) {
	//     	end = new Date().getTime();
	//   	}
	// },

	getInitialState: function() {
		return {
			authors: []
		};
	},


	componentDidMount: function() {
		// this.wait(6000);
		if (this.isMounted()) {
			this.setState({ authors: AuthorApi.getAllAuthors()});
		}
	},

	render: function() {
		return (
			<div>
				<h1> Authors </h1>
				<Link to="addAuthor" className="btn btn-default">Add Author</Link>
				<AuthorList authors={this.state.authors} />
			</div>
		);
	}
});

module.exports = AuthorPage;